using System;
using System.Collections.Generic;
using System.Reflection;
using RaftTheRyuzakiMode.Handlers;
using UnityEngine;

namespace RaftTheRyuzakiMode
{
    public class TR_ConsoleCommandManager
    {
        private static Dictionary<string, MethodInfo> ListMethosConsoleCommand;
        private static Queue<string> ListQueueConsoleCommands = new Queue<string>();
        private static int CountConsoleCommandInQueue = 0;
        
        public static ConsoleWindow ConsoleManager { get; private set; }

        public static void CreateConsole()
        {
            ConsoleManager = new ConsoleWindow();
            ConsoleManager.Initialize();
            ConsoleManager.SetTitle("Ruft Dedicated Server");
        }
        
        public static void Init()
        {
            if (ListMethosConsoleCommand == null)
            {
                ListMethosConsoleCommand = new Dictionary<string, MethodInfo>();
                Type[] types = typeof(TR_ConsoleCommandManager).Assembly.GetTypes();
                for (var i = 0; i < types.Length; i++)
                {
                    MethodInfo[] methods = types[i].GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.CreateInstance);
                    for (var j = 0; j < methods.Length; j++)
                    {
                        object[] attrs = methods[j].GetCustomAttributes(typeof(ConsoleCommandMethodAttribute), true);
                        if (attrs.Length != 0)
                        {
                            ConsoleCommandMethodAttribute attr = (ConsoleCommandMethodAttribute) attrs[0];
                            ListMethosConsoleCommand[attr.Command] = methods[j];
                        }
                    }
                }
                Debug.Log($"TR_ConsoleCommandManager.Init() - Has been loaded {ListMethosConsoleCommand.Count} count console commands!");
            }
        }
        
        public static void UpdateQueueConsoleCommand()
        {
            if (CountConsoleCommandInQueue != 0)
            {
                lock (ListQueueConsoleCommands)
                {
                    while (ListQueueConsoleCommands.Count != 0)
                    {
                        string line = ListQueueConsoleCommands.Dequeue();
                        string[] args = line.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                        try
                        {
                            string command = args[0].ToLower();
                            line = ((line.Length > command.Length) ? line.Substring(command.Length + 1) : string.Empty);
                            if (ListMethosConsoleCommand.TryGetValue(command, out MethodInfo method))
                            {
                                method.Invoke(null, new object[] {command, line, args});
                            } else
                                Debug.LogError($"TR_ConsoleCommandManager - This command [{command}] method is not found!");
                        }
                        catch (Exception ex)
                        {
                            Debug.LogException(ex);
                        }
                    }
                    CountConsoleCommandInQueue = 0;
                }
            }
        }

        public static void RunConsoleCommand(string line)
        {
            if (string.IsNullOrEmpty(line) == false)
            {
                lock (ListQueueConsoleCommands)
                {
                    ListQueueConsoleCommands.Enqueue(line);
                    CountConsoleCommandInQueue++;
                }
            } else
                Debug.LogWarning("Not use empty console command please!");
        }

        [AttributeUsage(AttributeTargets.Method)]
        public class ConsoleCommandMethodAttribute : Attribute
        {
            public String Command { get; }

            public ConsoleCommandMethodAttribute(string command)
            {
                this.Command = command.ToLower();
            }
        }
    }
}