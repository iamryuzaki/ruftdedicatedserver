using UnityEngine;

namespace RaftTheRyuzakiMode.ConsoleCommands
{
    public class TR_Mode
    {
        [TR_ConsoleCommandManager.ConsoleCommandMethodAttribute("exit")]
        public static void ExitCommand(string command, string line, string[] args)
        {
            Debug.Log("You use console command Exit!");
            Application.Quit();
        }
        
        [TR_ConsoleCommandManager.ConsoleCommandMethodAttribute("dump")]
        public static void Dumpommand(string command, string line, string[] args)
        {
            MonoBehaviour[] gObjects = GameObject.FindObjectsOfType<MonoBehaviour>();
            for (var i = 0; i < gObjects.Length; i++)
                Debug.Log("[Dump]: " + gObjects[i].GetType().FullName + " : " + gObjects[i].gameObject.name);
        }
    }
}