using System;
using System.Reflection;
using CSharpPatcher.SDK;
using RaftTheRyuzakiMode.Handlers;
using UnityEngine;

namespace RaftTheRyuzakiMode
{
    public class TR_GameManager
    {
        public static bool IsInitialized { get; private set; } = false;

        [HookMethod("Assembly-CSharp.dll", "", "SteamManager", "Awake", "self", HookMethod.ETypeInject.InjectMethod)]
        public static void Init(SteamManager gameManager)
        {
            if (IsInitialized == false)
            {
                IsInitialized = true;
                TR_ConsoleCommandManager.CreateConsole();
                TR_ConsoleCommandManager.Init();
                CreateWorker();
                Debug.Log("TR_GameManager::Init() - Completed");
            }
        }

        [HookMethod("Assembly-CSharp.dll", "", "Semih_Network", "Start", "self", HookMethod.ETypeInject.InjectMethod, 3)]
        public static void InitSemihNetwork(Semih_Network self)
        {
            Semih_Network.OnPlayerInitialized += player => { Debug.Log("OnPlayerInitialized: " + player.name); };
            global::GameManager.IsInNewGame = true;
            global::GameManager.FriendlyFire = true;
            global::SaveAndLoad.CurrentGameFileName = "Raft Dedicated Server";
            ComponentManager<Semih_Network>.Value.HostGame(RequestJoinAuthSetting.ALLOW_ALL, "");
        }

        [HookMethod("Assembly-CSharp.dll", "", "Settings", "Start", "", HookMethod.ETypeInject.InjectMethod, 11)]
        public static void Settings_Start()
        {
            // WTF?! My BuildID = 0 O.o - this is bug fix!
            Settings.AppBuildID = 3538192;
        }

        [HookMethod("Assembly-CSharp.dll", "", "Network_Player", "Start", "self", HookMethod.ETypeInject.RewriteMethod)]
        public static void NetworkPlayerCreated(Network_Player self)
        {
            Network_Player localPlayer = global::ComponentManager<global::Semih_Network>.Value.GetLocalPlayer();
            if (localPlayer == self)
            {
                Debug.Log("Network: OnInitializedLocalPlayer: " + self.name);
                self.Kill();   
            }
            else
            {
                Debug.Log("Network: OnConnectedPlayer: " + self.name);
                MethodInfo onWorldShiftMethod = typeof(Network_Player).GetMethod("OnWorldShift", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.CreateInstance);
                global::WorldShiftManager.OnWorldShift = (Action<Vector3>) Delegate.Combine((Delegate) global::WorldShiftManager.OnWorldShift, new Action<Vector3>(vector3 => { onWorldShiftMethod.Invoke(self, new object[] {vector3}); }));
                self.canvas = global::ComponentManager<global::CanvasHelper>.Value;
            }
        }
        
        private static void CreateWorker()
        {
            GameObject gObject = new GameObject();
            gObject.AddComponent<TR_GameObjectWorker>();
        }
    }
}