using System;
using System.Collections.Generic;
using System.Linq;
using RaftTheRyuzakiMode.Handlers;
using UnityEngine;

namespace RaftTheRyuzakiMode
{
    public class TR_GameObjectWorker : MonoBehaviour
    {
        public static TR_GameObjectWorker Instance { get; private set; }

        private List<float> ListFPSForAVG = new List<float>();
        
        private void Awake()
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
            this.name = "TR_GameObjectWorker";
        }

        private void Update()
        {
            this.ListFPSForAVG.Add((1 / Time.deltaTime));
            TR_ConsoleCommandManager.UpdateQueueConsoleCommand();
        }

        private void FixedUpdate()
        {
            if (this.ListFPSForAVG.Count != 0)
            {
                TR_ConsoleCommandManager.ConsoleManager.SetTitle($"Ruft Dedicated Server - FPS : {(int)this.ListFPSForAVG.Average()}");
                this.ListFPSForAVG.Clear();
            }
        }
    }
}