using CSharpPatcher.SDK;

namespace RaftTheRyuzakiMode
{
    public class TR_ReplaceForEmptyMethods
    {
        [HookMethod("Assembly-CSharp.dll", "", "VolumetricLightRenderer", "ChangeResolution", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void VolumetricLightRenderer_ChangeResolution()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "VolumetricLightRenderer", "Update", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void VolumetricLightRenderer_Update()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "VolumetricLightRenderer", "UpdateMaterialParameters", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void VolumetricLightRenderer_UpdateMaterialParameters()
        {  
        }
        [HookMethod("Assembly-CSharp.dll", "UltimateWater", "WaterSubsurfaceScattering", "Enable", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void UltimateWater_WaterSubsurfaceScattering_Enable()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "UltimateWater", "DepthModule", "Unbind", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void UltimateWater_DepthModule_Unbind()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "UltimateWater", "WaterVolumeProbe", "Start", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void UltimateWater_WaterVolumeProbe_Start()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "UltimateWater", "WaterVolumeProbe", "FixedUpdate", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void UltimateWater_WaterVolumeProbe_FixedUpdate()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "UltimateWater", "ProfilesManager", "ValidateProfiles", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void UltimateWater_ProfilesManager_ValidateProfiles()
        {
        }
        
        
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "Start", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_Start()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "Update", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_Update()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "SetOceanRoughness", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_SetOceanRoughness()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "SetOceanPlayerHeight", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_SetOceanPlayerHeight()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "PlayUI_OpenMenu", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_PlayUI_OpenMenu()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "PlayUI_MoveItem", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_PlayUI_MoveItem()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "PlayUI_Highlight", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_PlayUI_Highlight()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "PlayUI_Drop", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_PlayUI_Drop()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "PlayUI_Click_Fail", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_PlayUI_Click_Fail()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "PlayUI_Click", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_PlayUI_Click()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "PlayBlockBreak", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_PlayBlockBreak()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "OnSceneEvent", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_OnSceneEvent()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "OnMenuOpen", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_OnMenuOpen()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "OnMenuClose", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_OnMenuClose()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "OnDestroy", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_OnDestroy()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "HandleUnderWaterFilter", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_HandleUnderWaterFilter()
        {
        }
        [HookMethod("Assembly-CSharp.dll", "", "SoundManager", "GetOceanRoughness", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void SoundManager_GetOceanRoughness()
        {
        }
        
//        [HookMethod("Assembly-CSharp.dll", "", "SaveAndLoad", "RestoreRGDGame", "", HookMethod.ETypeInject.RewriteMethod)]
//        public static void SaveAndLoad_RestoreRGDGame()
//        {
//        }
    }
}