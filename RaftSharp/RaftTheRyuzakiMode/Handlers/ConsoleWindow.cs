using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.IO;
using System.Security;
using System.Threading;

namespace RaftTheRyuzakiMode.Handlers
{
	[SuppressUnmanagedCodeSecurity]
	public class ConsoleWindow
	{
		TextWriter oldOutput;

		public void Initialize()
		{
			if (!AttachConsole(0x0ffffffff))
			{
				AllocConsole();
			}

			oldOutput = Console.Out;
			try
			{
				System.Text.Encoding encoding = System.Text.Encoding.UTF8;

				System.Console.OutputEncoding = encoding;
				System.Console.InputEncoding = encoding;

				IntPtr stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
				Microsoft.Win32.SafeHandles.SafeFileHandle safeFileHandle = new Microsoft.Win32.SafeHandles.SafeFileHandle(stdHandle, true);
				FileStream fileStream = new FileStream(safeFileHandle, FileAccess.Write);

				StreamWriter standardOutput = new StreamWriter(fileStream, encoding);
				standardOutput.AutoFlush = true;
				Console.SetOut(standardOutput);
				
				stdHandle = GetStdHandle(STD_INPUT_HANDLE);
				safeFileHandle = new Microsoft.Win32.SafeHandles.SafeFileHandle(stdHandle, true);
				fileStream = new FileStream(safeFileHandle, FileAccess.Read);

				StreamReader standardInput = new StreamReader(fileStream, encoding);
				Console.SetIn(standardInput);

				ThreadPool.QueueUserWorkItem(_ =>
				{
					while (true)
					{
						string line = Console.ReadLine();
						TR_ConsoleCommandManager.RunConsoleCommand(line);
					}
				});
				
				
				Application.logMessageReceived += (text, trace, type) =>
				{
					switch (type)
					{
						case LogType.Error:
						case LogType.Exception:
						case LogType.Assert:
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine($"[{type}]: " + text);
							Console.WriteLine(trace);
							Console.ResetColor();
							break;
						case LogType.Warning:
							Console.ForegroundColor = ConsoleColor.Yellow;
							Console.WriteLine($"[{type}]: " + text);
							Console.WriteLine(trace);
							Console.ResetColor();
							break;
						default:
							Console.WriteLine($"[{type}]: " + text);
							break;
					}
				};
			}
			catch (System.Exception e)
			{
				Debug.Log("Couldn't redirect output: " + e.Message);
			}
		}

		public void Shutdown()
		{
			Console.SetOut(oldOutput);
			FreeConsole();
		}

		public void SetTitle(string strName)
		{
			SetConsoleTitleA(strName);
		}

		private const int STD_INPUT_HANDLE = -10;
		private const int STD_OUTPUT_HANDLE = -11;

		[DllImport("kernel32.dll", SetLastError = true)]
		static extern bool AttachConsole(uint dwProcessId);

		[DllImport("kernel32.dll", SetLastError = true)]
		static extern bool AllocConsole();

		[DllImport("kernel32.dll", SetLastError = true)]
		static extern bool FreeConsole();

		[DllImport("kernel32.dll", EntryPoint = "GetStdHandle", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
		private static extern IntPtr GetStdHandle(int nStdHandle);

		[DllImport("kernel32.dll")]
		static extern bool SetConsoleTitleA(string lpConsoleTitle);
	}
}